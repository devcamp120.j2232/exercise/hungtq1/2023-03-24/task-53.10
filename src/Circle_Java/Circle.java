package Circle_Java;

public class Circle {
    
    private double radius = 1.0;

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle() {
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea(){
        return Math.pow(this.radius, 2) * Math.PI;
    }
    
    public double getCircumference(){
        return 2 * this.radius * Math.PI;
    }

    public String toString(){
        return "Circle [radius = " + this.radius + "]";
    }

}
