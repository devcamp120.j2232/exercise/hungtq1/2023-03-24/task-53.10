import Circle_Java.Circle;

public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");
    
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(3);

        System.out.println("Circle1: ");
        System.out.println(circle1);
        System.out.println(circle1.getArea());
        System.out.println(circle1.getCircumference());
        System.out.println(circle1.toString());

        System.out.println("Circle2: ");
        System.out.println(circle2);
        System.out.println(circle2.getArea());
        System.out.println(circle2.getCircumference());
        System.out.println(circle2.toString());
    
    
    }
}
